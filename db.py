from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

_engine = create_engine('sqlite:///database.db')

# Works also with MySQL. This is how to initialize the engine in that case.
# The database schema must exist, tables are created automatically.
# _engine = create_engine('mysql+mysqlconnector://username:password@localhost/schema_name')

Session = sessionmaker(bind=_engine)
Base = declarative_base()

def create_tables_for_model_classes(classes):
    [cls.__table__.create(bind=_engine, checkfirst=True) for cls in classes]
