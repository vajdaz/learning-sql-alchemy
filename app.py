from model import Item, Relationship, initialize
from graphviz import Graph

if __name__ == "__main__":
    initialize()

    dot = Graph()

    for item in Item.find_all():
        dot.node(str(item.id), item.name)

    for relationship in Relationship.find_all():
        dot.edge(str(relationship.id_item_lhs), str(relationship.id_item_rhs),
            dir=relationship.direction.name, style=relationship.type.style,
            color=relationship.type.color)

    dot.render('graph.gv', view=True)
