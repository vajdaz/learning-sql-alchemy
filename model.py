from db import Base, Session, create_tables_for_model_classes
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sys import modules
from inspect import isclass
import csv

class Item(Base):
    __tablename__ = 'items'

    id = Column(Integer(), primary_key=True, autoincrement=True)
    name = Column(String(50), unique=True)

    def __init__(self, name):
        self.name = name

    @classmethod
    def find_by_name(cls, name):
        session = Session()
        try:
            return session.query(cls).filter(cls.name == name).one_or_none()
        finally:
            session.close()

    @classmethod
    def find_all(cls):
        session = Session()
        try:
            return session.query(cls).all()
        finally:
            session.close()

    @classmethod
    def load_from_csv(cls, file_path):
        with open(file_path, 'r') as items_file:
            reader = csv.DictReader(items_file)
            session = Session()
            try:
                for record in reader:
                    name = record['name']
                    item = cls.find_by_name(name)
                    if not item:
                        session.add(Item(name))
                session.commit()
            finally:
                session.close()

    @classmethod
    def count(cls):
        session = Session()
        try:
            return session.query(cls).count()
        finally:
            session.close()

    def __repr__(self):
        return 'Item(id={}, name={})'.format(self.id, self.name)


class DirectionType(Base):
    __tablename__ = 'direction_types'

    id = Column(Integer(), primary_key=True, autoincrement=False)
    name = Column(String(20), unique=True)

    def __init__(self, id, name):
        self.id = id
        self.name = name

    def save_to_db(self):
        session = Session()
        try:
            if not session.query(self.__class__).filter(self.__class__.id == self.id).one_or_none():
                session.add(self)
                session.commit()
        finally:
            session.close()

    @classmethod
    def count(cls):
        session = Session()
        try:
            return session.query(cls).count()
        finally:
            session.close()

    def __repr__(self):
        return 'DirectionType(id={}, name={})'.format(self.id, self.name)


class RelationshipType(Base):
    __tablename__ = 'relationship_types'

    id = Column(Integer(), primary_key=True, autoincrement=False)
    name = Column(String(50), unique=True)
    style = Column(String(20))
    color = Column(String(20))

    def __init__(self, id, name, style, color):
        self.id = id
        self.name = name
        self.style = style
        self.color = color

    def save_to_db(self):
        session = Session()
        try:
            if not session.query(self.__class__).filter(self.__class__.id == self.id).one_or_none():
                session.add(self)
                session.commit()
        finally:
            session.close()

    @classmethod
    def count(cls):
        session = Session()
        try:
            return session.query(cls).count()
        finally:
            session.close()

    def __repr__(self):
        return 'RelationshipType(id={}, name={})'.format(self.id, self.name)


class Relationship(Base):
    __tablename__ = 'relationships'

    id = Column(Integer(), primary_key=True, autoincrement=True)
    id_direction = Column(Integer(), ForeignKey('direction_types.id'))
    direction = relationship('DirectionType', lazy='joined')
    id_type = Column(Integer(), ForeignKey('relationship_types.id'))
    type = relationship('RelationshipType', lazy='joined')
    id_item_lhs = Column(Integer(), ForeignKey('items.id'))
    lhs = relationship('Item', foreign_keys=[id_item_lhs])
    id_item_rhs = Column(Integer(), ForeignKey('items.id'))
    rhs = relationship('Item', foreign_keys=[id_item_rhs])

    def __init__(self, id_item_lhs, id_item_rhs, id_type=0, id_direction=0):
        self.id_item_lhs = id_item_lhs
        self.id_item_rhs = id_item_rhs
        self.id_type = id_type
        self.id_direction = id_direction

    @classmethod
    def find_by_type(cls, type):
        session = Session()
        try:
            return session.query(cls).filter(cls.type == type).all()
        finally:
            session.close()

    @classmethod
    def count_matches(cls, dict):
        session = Session()
        try:
            q = session.query(cls)
            for colon_name, value in dict.items():
                q = q.filter(cls.__dict__[colon_name] == value)
            return q.count()
        finally:
            session.close()

    @classmethod
    def find_all(cls):
        session = Session()
        try:
            return session.query(cls).all()
        finally:
            session.close()

    def __repr__(self):
        return 'Relationship(id={})'.format(self.id)

    @classmethod
    def load_from_csv(cls, file_path):
        with open(file_path, 'r') as relationships_file:
            reader = csv.DictReader(relationships_file)
            session = Session()
            try:
                for record in reader:
                    if cls.count_matches(record) == 0:
                        session.add(Relationship(**record))
                session.commit()
            finally:
                session.close()

    @classmethod
    def count(cls):
        session = Session()
        try:
            return session.query(cls).count()
        finally:
            session.close()


default_direction_types = (
    DirectionType(0, "none"),
    DirectionType(1, "forward"),
    DirectionType(2, "back"),
    DirectionType(3, "both")
)


default_relationship_types = (
    RelationshipType(0, 'depends on', 'solid', 'black'),
    RelationshipType(1, 'exchanges data with', 'dashed', 'blue')
)

def create_default_objects():
    if DirectionType.count() == 0:
        for direction in default_direction_types:
            direction.save_to_db()
    if RelationshipType.count() == 0:
        for relationship_type in default_relationship_types:
            relationship_type.save_to_db()

def initialize():
    create_tables_for_model_classes([cls for name, cls in modules[__name__].__dict__.items() if (isclass(cls) and issubclass(cls, Base) and cls != Base)])
    create_default_objects()
    if Item.count() == 0:
        Item.load_from_csv('items.csv')
    if Relationship.count() == 0:
        Relationship.load_from_csv('relationships.csv')